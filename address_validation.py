import botocore
import boto3
import json
import urllib, xml.dom.minidom
import logging

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)

dynamodb = boto3.client('dynamodb')

def upload_invalid_address(table, address, count):
    try:
        response = dynamodb.put_item(
            TableName=table,
            Item={
                "Address": {
                    "S": address,
                },
                "Match_Count": {
                    "N": count
                }
            }
        )
        logger.info("{} added to {} in DynamoDB".format(address, table))
    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

def upload_valid_address(table, address, output):
    try:
        address_fields = output.split(',')
        logger.info(address_fields)
        street_addr = address_fields[0]
        city = address_fields[1]
        state = address_fields[2]
        zip_code = address_fields[3]

        response = dynamodb.put_item(
            TableName=table,
            Item={
                "Address": {
                    "S": address,
                },
                "Street_Address": {
                    "S": street_addr
                },
                "City": {
                    "S": city
                },
                "State": {
                    "S": state
                },
                "Zip_Code": {
                    "S": zip_code
                }
            }
        )
        logger.info("{} added to {} in DynamoDB".format(address, table))
    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

def get_matched_address(response):
    output = ""
    count = len(response)

    for item in response:
        if item['Next'] == "Retrieve":
            output =  item['Text'] + ", " + item['Description']

    return output, count

def validate_address(address):
    try:
        key = "WG98-CG94-PB99-NZ79"
        last_id = ""
        search_for = "Everything"
        country = "CAN"
        language_preference = "EN"
        max_suggestions = "7"
        max_results = "7"
        #Build the url
        requestUrl = "http://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Find/v2.10/xmla.ws?"
        requestUrl += "&" +  urllib.parse.urlencode({"Key":key})
        requestUrl += "&" +  urllib.parse.urlencode({"SearchTerm":address})
        requestUrl += "&" +  urllib.parse.urlencode({"LastId":last_id})
        requestUrl += "&" +  urllib.parse.urlencode({"SearchFor":search_for})
        requestUrl += "&" +  urllib.parse.urlencode({"Country":country})
        requestUrl += "&" +  urllib.parse.urlencode({"LanguagePreference":language_preference})
        requestUrl += "&" +  urllib.parse.urlencode({"MaxSuggestions":max_suggestions})
        requestUrl += "&" +  urllib.parse.urlencode({"MaxResults":max_results})

        #Get the data
        dataDoc = xml.dom.minidom.parseString(urllib.request.urlopen(requestUrl).read())

        #Get references to the schema and data
        schemaNodes = dataDoc.getElementsByTagName("Column")
        dataNotes = dataDoc.getElementsByTagName("Row")

        #Check for an error
        if len(schemaNodes) == 4 and schemaNodes[0].attributes["Name"].value == "Error":
            logger.error(dataNotes[0].attributes["Description"].value)

        #Work though the items in the response
        results = []
        for dataNode in dataNotes:
            rowData = dict()
            for schemaNode in schemaNodes:
                key = schemaNode.attributes["Name"].value
                value = dataNode.attributes[key].value
                rowData[key] = value
            results.append(rowData)

    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

    return results

def lambda_handler(event, context):
    try:
        logger.info(event)
        for event in event['Records']:
            event_type = event['eventName']
            if event_type == 'INSERT':
                valid_table = 'Validated_Addresses'
                invalid_table = 'Invalid_Addresses'
                address = event['dynamodb']['Keys']['Address']['S']
                if address != "":
                    response = validate_address(address)
                    output, count = get_matched_address(response)
                    if count == 1:
                        upload_valid_address(valid_table, address, output)
                    else:
                        upload_invalid_address(invalid_table, address, str(count))
                        logger.info("No match found or multiple suggestions found for {}".format(address))

        logger.info("Function Completed.")

    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return


    
