import botocore
import boto3
import csv
import json
import logging

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.client('s3')
dynamodb = boto3.client('dynamodb')
s3_resource = boto3.resource('s3')

def remove_file_s3(bucket, source_key):
    try:
        destination_key = source_key.replace('raw/','validated/')
        copy_source = {
            'Bucket': bucket,
            'Key': source_key
        }
        # Copy CSV file to Validate folder
        resp = s3_resource.meta.client.copy(copy_source, bucket, destination_key)
        # Delete CSV file from raw folder
        response = s3.delete_object(
                        Bucket=bucket,
                        Key=source_key
                    )
        logger.info("{} moved to {}".format(source_key, destination_key))
    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

def upload_to_dynamodb(table, address):
    try:
        response = dynamodb.put_item(
            TableName=table,
            Item={
                "Address": {
                    "S": address,
                }
            }
        )
    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

def get_address(raw_file):
    try:
        addresses = []
        file_content = raw_file['Body'].read().splitlines(True)
        logger.info("Raw File Content: {}".format(file_content))

        for address in file_content:
            addresses.append(address.strip().decode('utf-8'))

    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return

    return(addresses)

def get_file_from_s3(bucket_name, object_key):
    try:
        json_file = s3.get_object(
            Bucket=bucket_name,
            Key=object_key
        )
    except botocore.exceptions.ClientError as e:
        logger.error("Bucket {} not found".format(bucket_name))
        return
    return json_file

def lambda_handler(event, context):
    try:
        # logger.info(event)
        table = 'Addresses_Staging'

        bucket = event['Records'][0]['s3']['bucket']['name']
        source_key = event['Records'][0]['s3']['object']['key']
        # Get file from S3 bucket
        raw_file = get_file_from_s3(bucket, source_key)
        # Get addresses from CSV file
        addresses = get_address(raw_file)

        # Add address to DynamoDB
        for address in addresses:
            upload_to_dynamodb(table, address)
            logger.info("Added to DynamoDB: {}".format(address))

        # Move CSV file from raw to validated folder
        remove_file_s3(bucket, source_key)
        logger.info("Function Completed.")
    except botocore.exceptions.ClientError as e:
        logger.error(e.response['Error']['Message'])
        return


    
