import urllib, xml.dom.minidom
import csv

def write_to_csv(matched_addresses):
    with open('output.csv', 'w') as csvFile:
        fields = ['mls-addr', 'can-post-addr', 'matches']
        writer = csv.DictWriter(csvFile, fieldnames=fields)
        writer.writeheader()
        writer.writerows(matched_addresses)
        csvFile.close()

def validate_address(address):
    key = "WG98-CG94-PB99-NZ79"
    last_id = ""
    search_for = "Everything"
    country = "CAN"
    language_preference = "EN"
    max_suggestions = "7"
    max_results = "7"
    #Build the url
    requestUrl = "http://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Find/v2.10/xmla.ws?"
    requestUrl += "&" +  urllib.urlencode({"Key":key})
    requestUrl += "&" +  urllib.urlencode({"SearchTerm":address})
    requestUrl += "&" +  urllib.urlencode({"LastId":last_id})
    requestUrl += "&" +  urllib.urlencode({"SearchFor":search_for})
    requestUrl += "&" +  urllib.urlencode({"Country":country})
    requestUrl += "&" +  urllib.urlencode({"LanguagePreference":language_preference})
    requestUrl += "&" +  urllib.urlencode({"MaxSuggestions":max_suggestions})
    requestUrl += "&" +  urllib.urlencode({"MaxResults":max_results})

    #Get the data
    dataDoc = xml.dom.minidom.parseString(urllib.urlopen(requestUrl).read())

    #Get references to the schema and data
    schemaNodes = dataDoc.getElementsByTagName("Column")
    dataNotes = dataDoc.getElementsByTagName("Row")

    #Check for an error
    if len(schemaNodes) == 4 and schemaNodes[0].attributes["Name"].value == "Error":
     raise Exception, dataNotes[0].attributes["Description"].value

    #Work though the items in the response
    results = []
    for dataNode in dataNotes:
     rowData = dict()
     for schemaNode in schemaNodes:
          key = schemaNode.attributes["Name"].value
          value = dataNode.attributes[key].value
          rowData[key] = value
     results.append(rowData)

    return results

def query_address(address_list):
    output_addresses = []
    description = ""
    text = ""
    for address in address_list:
        print("Validating address: " + address)
        response = validate_address(address)
        matches = 0
        address_match = {}
        address_match['mls-addr'] = address
        for item in response:
            matches += 1
            if item['Next'] == "Retrieve":
                text = item['Text']
                description = item['Description']
                address_match['can-post-addr'] = text + ", " + description
        address_match['matches'] = str(matches)
        output_addresses.append(address_match)
    return output_addresses

def get_address():
    addresses = []
    with open('mls-input-data2.csv', 'r') as csv_file:
        csv_file.next()
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
             addresses.append(row[0].strip())
        return(addresses)

if __name__ == "__main__":
    address_list = []
    matched_addresses = {}

    address_list = get_address()
    matched_addresses = query_address(address_list)
    write_to_csv(matched_addresses)
